var navBar = document.getElementsByClassName('navbar-container')[0];
var navBarTitleContainer = document.getElementsByClassName('navbar__title-container')[0];
var navBarButtons = document.getElementsByClassName('navbar__buttons--with-dropdown');
var title = document.getElementsByClassName('title')[0];
var hamburgerButton = document.getElementsByClassName('navbar__hamburger')[0];

title.style.lineHeight = navBarTitleContainer.clientHeight + 'px';
var titleContent = title.innerHTML.split(' ');
var newTitleContent = [];
newTitleContent[0] = titleContent[0];
newTitleContent[1] = '<span class="subtitle">';

for(var i = 1; i < titleContent.length; i++) {
	newTitleContent[i + 1] = titleContent[i];
}
newTitleContent[i + 1] = '</span>';
title.innerHTML = newTitleContent.join(' ');

function bodyScrolled() {
	if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10) {
		navBar.className = 'navbar-container--collapsed';
		
	} else {
		navBar.className = 'navbar-container';
	}
}

function toggleNavMenu() {
	var subtitleText = document.getElementsByClassName('subtitle')[0];
	if(navBarButtons[0].style.display == 'none' || navBarButtons[0].style.display == '') {
		var newDisplayMode = 'block';
		navBar.className = 'navbar-container--collapsed';
		hamburgerButton.innerHTML = '<i class="material-icons">close</i>';
		if(subtitleText != undefined ) {
			subtitleText.style.visibility = 'hidden';	
		}
	} else {
		var newDisplayMode = 'none';	
		navBar.className = 'navbar-container';
		hamburgerButton.innerHTML = '<i class="material-icons">menu</i>';
		bodyScrolled();
		if(subtitleText != undefined ) {
			subtitleText.style.visibility = 'visible';	
		}
	}
	for (var navBarButton of navBarButtons) {
		navBarButton.style.display = newDisplayMode;
	}	
	initNav();
}

function initNav() {
	for (let navBarButton of navBarButtons) {
		navBarButton.style.transform = 'translateX(-' + (navBarButton.offsetLeft - 20) +  'px)';
		navBarButton.addEventListener('click', function() { 
			for (var subPanel of document.getElementsByClassName('navbar__submenu-container')) {
				subPanel.style.display = 'none';
			}
			var currentSubPanel = document.getElementById('nav-sub-' + navBarButton.id.split('-')[2]);
			currentSubPanel.style.display = 'block';
			var dropDown = document.getElementById('nav-dropdown-' + navBarButton.id.split('-')[2]);
			while(currentSubPanel.hasChildNodes()) {
				currentSubPanel.removeChild();	
			}
			for (var dropDownItem of dropDown.children) {
				var dropDownButton = document.createElement('BUTTON');
				dropDownButton.className = 'navbar__buttons--with-dropdown navbar__buttons--visible ';
				dropDownButton.textContent = dropDownItem.textContent;
				dropDownButton.onclick = dropDownItem.onclick;
				currentSubPanel.appendChild(dropDownButton);
			}
		});
		for (var subPanel of document.getElementsByClassName('navbar__submenu-container')) {
			subPanel.style.display = 'block';
			subPanel.style.transform = 'translateX(-' + (subPanel.offsetLeft - 40) +  'px)';
			subPanel.style.display = 'none';
		}
	}
}

for (var navBarButton of navBarButtons) {
	navBarButton.style.marginTop = (document.getElementsByClassName('navbar__buttons-container')[0].clientHeight - navBarButton.clientHeight)/2 + 'px';
}

/* Guide */

function filterPanels(elem) {
	var filterButtons = document.getElementsByClassName('filters__button');
	for (var filterButton of filterButtons) {
		if(filterButton.classList.contains('filters__button--active'))
			filterButton.classList.remove('filters__button--active');
	}
	elem.classList.add('filters__button--active');
	var newFilter = elem.id.split('').splice(15, elem.id.length).join('');
	
	var panels = document.getElementsByClassName('guide__project-cell');
	for (var panel of panels) {
		panel.style.display = 'none';
	}
	for (panel of panels) {
		var languages = JSON.parse(panel.onload.toString().replace(/^function\s*\S+\s*\([^)]*\)\s*\{|\}$/g, ""));
		languages = languages.map(function(value) { return value.toLowerCase()});
		if(languages.indexOf(newFilter) > -1 || newFilter == 'all') {
			panel.style.display = 'block';	
		}
	}
	
}

function loadIssues() {
	function onGet(content, issuePanel, titleParam, noParam, urlParam) {
		//Hide spinner
		document.getElementById('spinner--for-' + issuePanel.id).style.display = 'none';
		content = JSON.parse(content);
		var i = 0;
		for(let issue of content) {
			if( i > 4) break;

			var grid = document.createElement('DIV');
			grid.className = 'mdl-grid  guide__issue';

			var leftPanel = document.createElement('DIV');
			leftPanel.className = 'mdl-cell--10-col mdl-cell--3-col-phone';

			var rightPanel = document.createElement('DIV');
			rightPanel.className = 'mdl-cell--2-col mdl-cell--1-col-phone mdl-cell--middle';

			var title = document.createElement('P');
			title.className = 'guide__issue__title';
			var issueNumber = document.createElement('P');
			issueNumber.className = 'guide__issue__number';
			issueNumber.textContent = issue[noParam];
			title.textContent = issue[titleParam];

			issueNumber.appendChild(title);
			leftPanel.appendChild(issueNumber);	

			var link = document.createElement('BUTTON');
			link.className = 'mdl-button mdl-js-button mdl-button--icon mdl-button--colored';
			link.innerHTML = '<i class="material-icons">launch</i>';
			link.addEventListener('click', function(e) {openURL(issue[urlParam], '_blank');});

			rightPanel.appendChild(link);

			grid.appendChild(leftPanel);
			grid.appendChild(rightPanel);
			issuePanel.appendChild(grid);
			
			i++;
		}
		
		var issuesLink = document.createElement('BUTTON');
		issuesLink.className = 'mdl-button mdl-js-button mdl-js-ripple-effect guide__issues__link';
		issuesLink.textContent = 'View all';
		var repoURL = content[0][urlParam].split('/');
		repoURL.pop();
		issuesLink.addEventListener('click', function(e) {openURL(repoURL.join('/'), '_blank');});
		issuePanel.appendChild(issuesLink);
			
		componentHandler.upgradeDom();
	}
	var issuesPanels = document.getElementsByClassName('guide__issues');
	for (let issuePanel of issuesPanels) {
		if(isNaN(issuePanel.id)) {	
			//getIssues('https://api.github.com/repos/' + issuePanel.id +'/issues?state=open', function(content) {onGet(content, issuePanel, 'title', 'number', 'html_url')});
			//commented to avoid rate limiting during testing
		} else {
			
			getIssues('https://gitlab.com/api/v4/projects/' + issuePanel.id +'/issues?state=opened', function(content) {onGet(content, issuePanel, 'title', 'iid', 'web_url')});	
		}
	}
}

function getIssues(url, callback) {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() { 
		if (xmlHttp.status == 200 && xmlHttp.readyState == 4) {
			callback(xmlHttp.responseText);
		}
	}
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}
loadIssues();