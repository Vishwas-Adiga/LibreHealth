var currentFeatureWeight = 1;

function showFeatureByWeight(featureWeight) {
	for (var featureContainer of document.getElementsByClassName('feature-container')) {
		featureContainer.style.display = 'none';
	}
	
	for (var featuresTitle of document.getElementsByClassName('features__title')) {
		featuresTitle.className = 'mdl-button mdl-js-button mdl-button--icon mdl-button--colored features__title';
	}
	document.getElementById('feature-container--for-' + featureWeight).style.display = 'block';
	document.getElementById('features-title--for-' + featureWeight).className = 'mdl-button mdl-js-button mdl-button--icon mdl-button--colored features__title feature--focussed';
	currentFeatureWeight = parseInt(featureWeight, 10);
}
showFeatureByWeight('1');

function showProjectByTitle(projectTitle) {
	for (var project of document.getElementsByClassName('project')) {
		project.style.display = 'none';
	}
	document.getElementById('project--for-' + projectTitle).style.display = 'block';
	for (var projectButton of document.getElementsByClassName('project__button')) {
		projectButton.className = 'mdl-button mdl-js-button mdl-js-ripple-effect project__button';
	}
	document.getElementById('project__button--for-' + projectTitle).className = 'mdl-button mdl-js-button mdl-js-ripple-effect project__button project__button--focussed';
}
showProjectByTitle('LibreHealth EHR');	

for (var teamName of document.getElementsByClassName('team__name')) {
	teamName.innerHTML = teamName.innerHTML.toLowerCase();
}

setInterval(function() {
	currentFeatureWeight += 1;
	currentFeatureWeight = currentFeatureWeight % (document.getElementsByClassName('feature-container').length + 1);
	if (currentFeatureWeight == 0) currentFeatureWeight = 1;
  showFeatureByWeight(currentFeatureWeight);
}, 5000);